package org.alexsem.dice.ui.input;

import org.alexsem.dice.game.HandMask;
import org.alexsem.dice.game.PileMask;
import org.alexsem.dice.ui.Action;
import org.alexsem.dice.ui.ActionList;
import org.jetbrains.annotations.NotNull;

public class DroidGameInteractor implements GameInteractor {
    @NotNull
    @Override
    public Action pickDiceFromHand(@NotNull HandMask handMask, @NotNull ActionList actionList) {
        return null;
    }

    @NotNull
    @Override
    public Action pickDiceFromPile(@NotNull PileMask pileMask, @NotNull ActionList actionList) {
        return null;
    }

    @Override
    public void anyInput() {

    }

    @NotNull
    @Override
    public Action pickAction(@NotNull ActionList actionList) {
        return null;
    }

    @NotNull
    @Override
    public Action pickItemFromList(int i) {
        return null;
    }
}
