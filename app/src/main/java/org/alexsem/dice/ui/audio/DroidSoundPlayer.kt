package org.alexsem.dice.ui.audio

import android.content.Context
import android.media.AudioManager
import android.media.SoundPool
import java.util.concurrent.locks.ReentrantReadWriteLock

/**
 * Class used for playing sequence sounds
 */

class DroidSoundPlayer(context: Context) : SoundPlayer {

    private val soundPool: SoundPool = SoundPool(2, AudioManager.STREAM_MUSIC, 100)
    private val sounds = mutableMapOf<Sound, Int>()
    private val rate = 1f

    private val lock = ReentrantReadWriteLock()

    init {
        Thread(SoundLoader(context)).start()
    }

    override fun play(sound: Sound) {
        if (lock.readLock().tryLock()) {
            try {
                sounds[sound]?.let { s->
                    soundPool.play(s, 1f, 1f, 1, 0, rate)
                }
            } finally {
                lock.readLock().unlock()
            }
        }
    }

    //----------------------------------------------------------------------------------------------

    /**
     * Class used to load sounds in background
     */
    private inner class SoundLoader(private val context: Context) : Runnable {

        override fun run() {
            val assets = context.assets
            lock.writeLock().lock()
            try {
                Sound.values().forEach { s ->
                    sounds[s] = soundPool.load(
                        assets.openFd("sound/${s.toString().toLowerCase()}.wav"), 1
                    )
                }
            } finally {
                lock.writeLock().unlock()
            }
        }
    }
}
