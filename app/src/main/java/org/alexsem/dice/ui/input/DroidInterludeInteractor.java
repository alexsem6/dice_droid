package org.alexsem.dice.ui.input;

import org.alexsem.dice.ui.Action;
import org.alexsem.dice.ui.ActionList;
import org.jetbrains.annotations.NotNull;

public class DroidInterludeInteractor implements InterludeInteractor {
    @NotNull
    @Override
    public Action advanceScript() {
        return null;
    }

    @Override
    public void anyInput() {

    }

    @NotNull
    @Override
    public Action pickAction(@NotNull ActionList actionList) {
        return null;
    }

    @NotNull
    @Override
    public Action pickItemFromList(int i) {
        return null;
    }
}
