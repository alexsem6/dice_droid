package org.alexsem.dice.ui.render

import org.alexsem.dice.ui.DiceSurface
import org.alexsem.dice.ui.render.strings.StringLoadHelper
import org.alexsem.dice.ui.render.strings.StringLoader

/**
 * Superclass for all Droid renderers
 */
abstract class DroidRenderer(protected val surface: DiceSurface, private val loader: StringLoader) {

    protected val helper = StringLoadHelper(loader)

    protected fun loadString(key: String) = loader.loadString(key)

    protected fun mergeActionData(data: Array<String>) = if (data.size > 1) {
        if (data[1].first().isLowerCase()) data[0] + data[1] else data[1]
    } else data.getOrNull(0) ?: ""

}