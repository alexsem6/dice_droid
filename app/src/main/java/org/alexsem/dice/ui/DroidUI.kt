package org.alexsem.dice.ui

import org.alexsem.dice.ui.input.*
import org.alexsem.dice.ui.render.*
import org.alexsem.dice.ui.render.strings.StringLoader
import org.alexsem.dice.ui.storage.DroidStorage
import java.util.concurrent.BlockingQueue

class DroidUI(surface: DiceSurface, loader: StringLoader): UI {

    override val gameRenderer = DroidGameRenderer()
    override val interludeRenderer = DroidInterludeRenderer()
    override val managerRenderer = DroidManagerRenderer()
    override val menuRenderer = DroidMenuRenderer(surface, loader)
    override val upgradeRenderer = DroidUpgradeRenderer()

    override val gameInteractor = DroidGameInteractor()
    override val interludeInteractor = DroidInterludeInteractor()
    override val managerInteractor = DroidManagerInteractor()
    override val menuInteractor = DroidMenuInteractor(surface)
    override val upgradeInteractor = DroidUpgradeInteractor()

    override val storage =  DroidStorage()

}