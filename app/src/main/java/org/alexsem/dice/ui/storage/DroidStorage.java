package org.alexsem.dice.ui.storage;

import org.alexsem.dice.generator.template.adventure.AdventureTemplate;
import org.alexsem.dice.model.Hero;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.List;

public class DroidStorage implements Storage {
    @NotNull
    @Override
    public PartyInfoFull createParty(@NotNull String s, @NotNull List<Hero> list, @NotNull AdventureTemplate adventureTemplate) throws IOException {
        return null;
    }

    @Override
    public void deleteGame(@NotNull String s) throws IOException {

    }

    @Override
    public void deleteParty(@NotNull String s) throws IOException {

    }

    @NotNull
    @Override
    public String generatePartyIdentifier(@NotNull String s) {
        return null;
    }

    @Override
    public boolean isGameSaveAvailable(@NotNull String s) {
        return false;
    }

    @Override
    public boolean isPartiesAvailable() {
        return false;
    }

    @NotNull
    @Override
    public GameInfo loadGame(@NotNull String s) throws IOException {
        return null;
    }

    @NotNull
    @Override
    public PartyInfoFull loadParty(@NotNull String s) throws IOException {
        return null;
    }

    @NotNull
    @Override
    public List<PartyInfoShort> loadPartyList() throws IOException {
        return null;
    }

    @Override
    public void saveGame(@NotNull GameInfo gameInfo) throws IOException {

    }

    @Override
    public void updateParty(@NotNull PartyInfoFull partyInfoFull) throws IOException {

    }
}
