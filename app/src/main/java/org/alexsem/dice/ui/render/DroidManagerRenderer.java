package org.alexsem.dice.ui.render;

import org.alexsem.dice.model.Die;
import org.alexsem.dice.model.Hero;
import org.alexsem.dice.ui.ActionList;
import org.alexsem.dice.ui.StatusMessage;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class DroidManagerRenderer implements ManagerRenderer {
    @Override
    public int getCommonPoolDicePerPage() {
        return 0;
    }

    @Override
    public int getMaxHeroNameLength() {
        return 0;
    }

    @Override
    public int getMaxPartyNameLength() {
        return 0;
    }

    @Override
    public void drawDiceManagerScreen(@NotNull Hero hero, @NotNull Map<Die.Type, ? extends List<Die>> map, @Nullable Die.Type type, @NotNull List<Die> list, int i, int i1, @NotNull Set<Integer> set, @NotNull StatusMessage statusMessage, @NotNull ActionList actionList) {

    }

    @Override
    public void drawHeroClassSelectionDialog(@NotNull String s, @NotNull List<Hero> list, @NotNull List<? extends Hero.Type> list1, @NotNull StatusMessage statusMessage, @NotNull ActionList actionList) {

    }

    @Override
    public void drawHeroNameEditScreen(@NotNull String s, @NotNull List<Hero> list, int i, @NotNull Hero.Type type, @NotNull String s1, @NotNull StatusMessage statusMessage, @NotNull ActionList actionList) {

    }

    @Override
    public void drawPartyManagerGeneral(@NotNull String s, @NotNull List<Hero> list, @NotNull StatusMessage statusMessage, @NotNull ActionList actionList) {

    }

    @Override
    public void drawPartyManagerHeroSelection(@NotNull String s, @NotNull List<Hero> list, @NotNull StatusMessage statusMessage, @NotNull ActionList actionList) {

    }

    @Override
    public void drawPartyNameEditScreen(@NotNull String s, @NotNull StatusMessage statusMessage, @NotNull ActionList actionList) {

    }

    @Override
    public void clearScreen() {

    }
}
