package org.alexsem.dice.ui

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.view.MotionEvent
import android.view.View
import java.util.*
import java.util.concurrent.TimeUnit
import java.util.concurrent.BlockingQueue
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.LinkedBlockingQueue

typealias RenderInstructions = (Canvas, Paint) -> Unit

private class ActiveRect(val action: Action, left: Float, top: Float, right: Float, bottom: Float) {
    val rect = RectF(left, top, right, bottom)
    fun check(x: Float, y: Float, w: Float, h: Float) = rect.contains(x / w, y / h)
}

//----------------------------------------------------------------------------------------------------------------------

class DiceSurface(context: Context) : View(context) {

    private val actionQueue: BlockingQueue<Action> = LinkedBlockingQueue<Action>()
    private val rectangles: MutableSet<ActiveRect> = Collections.newSetFromMap(ConcurrentHashMap<ActiveRect, Boolean>())
    private var instructions: RenderInstructions = { _, _ -> }

    private val paint = Paint().apply {
        color = Color.YELLOW
        style = Paint.Style.STROKE
        isAntiAlias = true
    }

    //------------------------------------------------------------------------------------------------------------------

    fun updateInstructions(instructions: RenderInstructions) {
        this.instructions = instructions
        this.postInvalidate()
    }

    fun clearRectangles() {
        rectangles.clear()
    }

    fun addRectangle(action: Action, left: Float, top: Float, right: Float, bottom: Float) {
        rectangles.add(ActiveRect(action, left, top, right, bottom))
    }

    fun awaitAction(): Action = actionQueue.take()

    //------------------------------------------------------------------------------------------------------------------


    override fun onTouchEvent(event: MotionEvent): Boolean {
        if (event.action == MotionEvent.ACTION_UP) {
            with(rectangles.firstOrNull { it.check(event.x, event.y, width.toFloat(), height.toFloat()) }) {
                if (this != null) {
                    actionQueue.put(action)
                } else {
                    actionQueue.offer(Action(Action.Type.NONE), 200, TimeUnit.MILLISECONDS)
                }
            }
        }
        return true
    }

    //------------------------------------------------------------------------------------------------------------------

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        canvas.drawColor(Color.BLACK)
        instructions(canvas, paint)
    }

    //------------------------------------------------------------------------------------------------------------------
}