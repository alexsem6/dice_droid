package org.alexsem.dice.ui.render.strings

import android.content.Context

class ResourceStringLoader(context: Context) : StringLoader {

    private val packageName = context.packageName
    private val resources = context.resources

    override fun loadString(key: String): String =
        resources.getString(resources.getIdentifier(key, "string", packageName))
}