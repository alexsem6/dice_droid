package org.alexsem.dice.ui.input

import org.alexsem.dice.ui.Action
import org.alexsem.dice.ui.ActionList
import org.alexsem.dice.ui.DiceSurface

abstract class DroidInteractor(protected val surface: DiceSurface) : Interactor {

    override fun anyInput() {
        surface.awaitAction()
    }

    override fun pickAction(list: ActionList): Action {
        while (true) {
            val type = surface.awaitAction().type
            list
                .filter(Action::isEnabled)
                .find { it.type == type }
                ?.let { return it }
        }
    }

    override fun pickItemFromList(listSize: Int): Action {
        while (true) {
            surface.awaitAction().let {
                if (it.type == Action.Type.LIST_ITEM && it.data in 0 until listSize) {
                    return it
                }
            }
        }
    }
}