package org.alexsem.dice.ui.input;

import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import org.alexsem.dice.ui.Action;
import org.alexsem.dice.ui.ActionList;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Set;

public class DroidManagerInteractor implements ManagerInteractor {
    @Nullable
    @Override
    public String inputText(@NotNull String s, int i, boolean b, boolean b1, @Nullable Function1<? super String, Unit> function1) {
        return null;
    }

    @NotNull
    @Override
    public Action pickDiceManagerAction(@NotNull ActionList actionList, @NotNull Set<Integer> set) {
        return null;
    }

    @Override
    public void anyInput() {

    }

    @NotNull
    @Override
    public Action pickAction(@NotNull ActionList actionList) {
        return null;
    }

    @NotNull
    @Override
    public Action pickItemFromList(int i) {
        return null;
    }
}
