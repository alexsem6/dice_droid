package org.alexsem.dice.ui.render;

import org.alexsem.dice.game.*;
import org.alexsem.dice.model.*;
import org.alexsem.dice.ui.ActionList;
import org.alexsem.dice.ui.StatusMessage;
import org.alexsem.dice.ui.render.GameRenderer;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Map;

public class DroidGameRenderer implements GameRenderer {
    @Override
    public int calculateHeroInfoSkillsPageCount(@NotNull Hero hero) {
        return 0;
    }

    @Override
    public int calculateLocationInfoGeneralPageCount(@NotNull Location location) {
        return 0;
    }

    @Override
    public int calculateLocationInfoRulesPageCount(@NotNull Location location) {
        return 0;
    }

    @Override
    public int calculateScenarioInfoAlliesPageCount(@NotNull Scenario scenario) {
        return 0;
    }

    @Override
    public int calculateScenarioInfoGeneralPageCount(@NotNull Scenario scenario) {
        return 0;
    }

    @Override
    public int calculateScenarioInfoRulesPageCount(@NotNull Scenario scenario) {
        return 0;
    }

    @Override
    public int calculateScenarioScenicTextPageCount(@NotNull Scenario scenario, boolean b) {
        return 0;
    }

    @Override
    public void drawEnemyInfo(@NotNull Enemy enemy, @NotNull StatusMessage statusMessage, @NotNull ActionList actionList) {

    }

    @Override
    public void drawGameLoss(@NotNull StatusMessage statusMessage) {

    }

    @Override
    public void drawGamePauseMenu(@NotNull StatusMessage statusMessage, @NotNull ActionList actionList) {

    }

    @Override
    public void drawGameVictory(@NotNull StatusMessage statusMessage) {

    }

    @Override
    public void drawHeroDeath(@NotNull Hero hero) {

    }

    @Override
    public void drawHeroInfoDice(@NotNull Hero hero, @NotNull StatusMessage statusMessage, @NotNull ActionList actionList) {

    }

    @Override
    public void drawHeroInfoGeneral(@NotNull Hero hero, @NotNull StatusMessage statusMessage, @NotNull ActionList actionList) {

    }

    @Override
    public void drawHeroInfoSkills(@NotNull Hero hero, int i, int i1, @NotNull StatusMessage statusMessage, @NotNull ActionList actionList) {

    }

    @Override
    public void drawHeroSelectionDialog(@NotNull List<Hero> list, @NotNull StatusMessage statusMessage, @NotNull ActionList actionList) {

    }

    @Override
    public void drawHeroTurnStart(@NotNull Hero hero) {

    }

    @Override
    public void drawLocationInfoGeneral(@NotNull Location location, int i, int i1, @NotNull StatusMessage statusMessage, @NotNull ActionList actionList) {

    }

    @Override
    public void drawLocationInfoRules(@NotNull Location location, int i, int i1, @NotNull StatusMessage statusMessage, @NotNull ActionList actionList) {

    }

    @Override
    public void drawLocationInteriorScreen(@NotNull Location location, @NotNull List<Hero> list, int i, @NotNull Hero hero, @Nullable DieBattleCheck dieBattleCheck, @Nullable DiePair diePair, @Nullable Threat threat, @NotNull HandMask handMask, @NotNull HandMask handMask1, @NotNull StatusMessage statusMessage, @NotNull ActionList actionList) {

    }

    @Override
    public void drawLocationSelectionDialog(@NotNull Scenario scenario, int i, @NotNull List<Location> list, @NotNull Map<Location, ? extends List<Hero>> map, @NotNull Hero hero, @NotNull StatusMessage statusMessage, @NotNull ActionList actionList) {

    }

    @Override
    public void drawLocationTravelScreen(@NotNull Location location, @NotNull Location location1, @NotNull Hero hero, int i, @NotNull StatusMessage statusMessage, @NotNull ActionList actionList) {

    }

    @Override
    public void drawModifiersInfo(@NotNull Hero hero, @NotNull List<PendingSkillModifier> list, @NotNull List<PendingAllyModifier> list1, @NotNull StatusMessage statusMessage, @NotNull ActionList actionList) {

    }

    @Override
    public void drawObstacleInfo(@NotNull Obstacle obstacle, @NotNull StatusMessage statusMessage, @NotNull ActionList actionList) {

    }

    @Override
    public void drawPileDiceSelectionDialog(@NotNull Pile pile, @NotNull PileMask pileMask, @NotNull PileMask pileMask1, @NotNull StatusMessage statusMessage, @NotNull ActionList actionList) {

    }

    @Override
    public void drawScenarioInfoAllies(@NotNull Scenario scenario, int i, int i1, @NotNull StatusMessage statusMessage, @NotNull ActionList actionList) {

    }

    @Override
    public void drawScenarioInfoGeneral(@NotNull Scenario scenario, @NotNull Pile pile, int i, int i1, @NotNull StatusMessage statusMessage, @NotNull ActionList actionList) {

    }

    @Override
    public void drawScenarioInfoRules(@NotNull Scenario scenario, int i, int i1, @NotNull StatusMessage statusMessage, @NotNull ActionList actionList) {

    }

    @Override
    public void drawScenarioScenicText(@NotNull Scenario scenario, boolean b, int i, int i1, @NotNull ActionList actionList) {

    }

    @Override
    public void drawSkillSelectionDialog(@NotNull List<Skill> list, @NotNull List<AllySkill> list1, @NotNull StatusMessage statusMessage, @NotNull ActionList actionList) {

    }

    @Override
    public void drawVillainInfo(@NotNull Villain villain, @NotNull StatusMessage statusMessage, @NotNull ActionList actionList) {

    }

    @Override
    public void clearScreen() {

    }
}
