package org.alexsem.dice.ui.input;

import org.alexsem.dice.ui.Action;
import org.alexsem.dice.ui.ActionList;
import org.jetbrains.annotations.NotNull;

public class DroidUpgradeInteractor implements UpgradeInteractor {
    @Override
    public void anyInput() {

    }

    @NotNull
    @Override
    public Action pickAction(@NotNull ActionList actionList) {
        return null;
    }

    @NotNull
    @Override
    public Action pickItemFromList(int i) {
        return null;
    }
}
