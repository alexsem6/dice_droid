package org.alexsem.dice.ui.render;

import org.alexsem.dice.model.Die;
import org.alexsem.dice.model.Hero;
import org.alexsem.dice.model.Skill;
import org.alexsem.dice.ui.ActionList;
import org.alexsem.dice.ui.StatusMessage;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class DroidUpgradeRenderer implements UpgradeRenderer {
    @Override
    public void drawDieTypeSelectionDialog(@NotNull Hero hero, @NotNull StatusMessage statusMessage, @NotNull ActionList actionList) {

    }

    @Override
    public void drawHeroDiceLimits(@NotNull Hero hero, @Nullable Die.Type type, @NotNull StatusMessage statusMessage, @NotNull ActionList actionList) {

    }

    @Override
    public void drawHeroHandCapacityIncrease(@NotNull Hero hero, @NotNull StatusMessage statusMessage) {

    }

    @Override
    public void drawHeroSkills(@NotNull Hero hero, @NotNull List<Skill> list, @NotNull StatusMessage statusMessage, @NotNull ActionList actionList) {

    }

    @Override
    public void drawNewDieReceived(@NotNull Hero hero, @NotNull Die die) {

    }

    @Override
    public void drawSkillInfo(@NotNull Skill skill, @NotNull StatusMessage statusMessage, @NotNull ActionList actionList) {

    }

    @Override
    public void clearScreen() {

    }
}
