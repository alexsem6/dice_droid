package org.alexsem.dice.ui

import android.app.Activity
import android.os.Bundle
import org.alexsem.dice.menu.MainMenu
import org.alexsem.dice.ui.audio.Audio
import org.alexsem.dice.ui.audio.DroidMusicPlayer
import org.alexsem.dice.ui.audio.DroidSoundPlayer
import org.alexsem.dice.ui.render.strings.ResourceStringLoader

class MainActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Audio.init(DroidSoundPlayer(this), DroidMusicPlayer(this))

        val surface = DiceSurface(this)
        val ui = DroidUI(surface, ResourceStringLoader(this))

        setContentView(surface)

        Thread {
            MainMenu(ui).start()
            finish()
        }.start()
    }

    override fun onBackPressed() {
    }
}
