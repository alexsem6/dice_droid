package org.alexsem.dice.ui.audio

import android.content.Context
import android.media.AudioManager
import android.media.MediaPlayer

class DroidMusicPlayer(private val context: Context): MusicPlayer {

    private var currentMusic: Music? = null
    private val player = MediaPlayer()

    override fun play(music: Music) {
        if (currentMusic == music) {
            return
        }
        currentMusic = music

        player.setAudioStreamType(AudioManager.STREAM_MUSIC)
        val afd = context.assets.openFd("music/${music.toString().toLowerCase()}.mp3")
        player.setDataSource(afd.fileDescriptor, afd.startOffset, afd.length)
        player.setOnCompletionListener {
            it.seekTo(0)
            it.start()
        }
        player.prepare()
        player.start()
    }

    override fun stop() {
        currentMusic = null
        player.release()
    }


}
