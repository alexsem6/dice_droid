package org.alexsem.dice.ui.render;

import org.alexsem.dice.model.Interlude;
import org.jetbrains.annotations.NotNull;

public class DroidInterludeRenderer implements InterludeRenderer {
    @Override
    public void drawHeadline(@NotNull Interlude interlude, @NotNull Interlude.Event.Headline headline) {

    }

    @Override
    public void drawNarration(@NotNull Interlude interlude, @NotNull Interlude.Event.Narration narration) {

    }

    @Override
    public void drawPhrase(@NotNull Interlude interlude, @NotNull Interlude.Event.Phrase phrase) {

    }

    @Override
    public void clearScreen() {

    }
}
