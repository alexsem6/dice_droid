package org.alexsem.dice.ui.render

import android.graphics.Color
import org.alexsem.dice.model.Hero
import org.alexsem.dice.model.LocalizedString
import org.alexsem.dice.ui.ActionList
import org.alexsem.dice.ui.DiceSurface
import org.alexsem.dice.ui.StatusMessage
import org.alexsem.dice.ui.render.strings.StringLoader
import org.alexsem.dice.ui.storage.PartyInfoShort

class DroidMenuRenderer(surface: DiceSurface, loader: StringLoader) : DroidRenderer(surface, loader), MenuRenderer {

    override fun clearScreen() {
        surface.clearRectangles()
        surface.updateInstructions { _, _ -> }
    }

    override fun drawMainMenu(actions: ActionList) {
        //Prepare rectangles
        surface.clearRectangles()
        val percentage = 1.0f / actions.size
        actions.forEachIndexed { i, a ->
            surface.addRectangle(a, i * percentage, 0.45f, i * percentage + percentage, 1f)
        }
        //Prepare instructions
        surface.updateInstructions { c, p ->
            val canvasWidth = c.width
            val canvasHeight = c.height
            val buttonTop = canvasHeight * 0.45f
            val buttonWidth = canvasWidth / actions.size
            val padding = canvasHeight / 144f
            //Draw title text
            p.textSize = canvasHeight / 3f
            p.strokeWidth = 0f
            p.color = Color.parseColor("#ff808000")
            p.isFakeBoldText = true
            c.drawText(
                "DICE",
                (canvasWidth - p.measureText("DICE")) / 2f,
                (buttonTop - p.ascent() - p.descent()) / 2f,
                p
            )
            p.isFakeBoldText = false
            //Draw action buttons
            p.textSize = canvasHeight / 24f
            actions.forEachIndexed { i, a ->
                p.color = if (a.isEnabled) Color.YELLOW else Color.LTGRAY
                p.strokeWidth = canvasHeight / 240f
                c.drawRect(
                    i * buttonWidth + padding,
                    buttonTop + padding,
                    i * buttonWidth + buttonWidth - padding,
                    canvasHeight - padding,
                    p
                )
                val name = mergeActionData(helper.loadActionData(a))
                p.strokeWidth = 0f
                c.drawText(
                    name,
                    i * buttonWidth + (buttonWidth - p.measureText(name)) / 2f,
                    (canvasHeight + buttonTop - p.ascent() - p.descent()) / 2f,
                    p
                )
            }
        }
    }

    //------------------------------------------------------------------------------------------------------------------

    override val partiesPerPage = 0 //TODO not implemented
    override val scenariosPerPage = 0 //TODO not implemented

    override fun drawAdventureList(
        names: List<LocalizedString>,
        descriptions: List<LocalizedString>,
        selectedIndex: Int,
        statusMessage: StatusMessage,
        actions: ActionList
    ) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun drawPartyList(
        parties: List<PartyInfoShort>,
        pickedIndex: Int?,
        page: Int,
        totalPages: Int,
        statusMessage: StatusMessage,
        actions: ActionList
    ) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun drawScenarioList(
        adventureName: LocalizedString,
        phaseNames: List<LocalizedString>,
        progress: Int,
        heroes: List<Hero>,
        pickedIndex: Int?,
        page: Int,
        totalPages: Int,
        statusMessage: StatusMessage,
        actions: ActionList
    ) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}