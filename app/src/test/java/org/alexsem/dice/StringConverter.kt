package org.alexsem.dice

import org.junit.Ignore
import org.junit.Test

import java.lang.StringBuilder
import java.nio.file.Files
import java.nio.file.Paths

class StringConverter {

    @Test
    @Ignore
    fun convert() {
        val sourceDir = Paths.get("D:", "sem", "DiceCli", "src", "main", "resources", "text")
        val destDir = Paths.get("D:", "000")

        val filterFile = "strings(_\\w{2})?\\.properties".toRegex()
        val replacerDir = "values$1"

        val mapperComment = "# ?(.*)".toRegex()
        val replacerComment = "    <!-- $1 -->"
        val mapperString = "^([^ =]*?)=(.*?)$".toRegex()
        val replacerString = "    <string name=\"$1\">$2</string>"
        val mapperApos = "(\\w)'(\\w)".toRegex()
        val replaceApos = "$1\\\'$2"


        Files.list(sourceDir).filter { it.fileName.toString().matches(filterFile) }.forEach { f ->
            val dir = destDir.resolve(f.fileName.toString().replace(filterFile, replacerDir))
            Files.createDirectories(dir)
            Files.write(dir.resolve("dice_strings.xml"), Files.lines(f)
                .map { it.replace(mapperComment, replacerComment) }
                .map { it.replace(mapperString, replacerString) }
                .map { it.replace(mapperApos, replaceApos) }
                .collect(
                    { StringBuilder("<resources>\n") },
                    { sb, s -> sb.append(s).append('\n') },
                    { sb, sb0 -> sb.append(sb0).append('\n') })
                .append("</resources>")
                .toString()
                .toByteArray(Charsets.UTF_8)
            )
        }

    }

}